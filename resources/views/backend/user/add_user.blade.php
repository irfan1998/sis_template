@extends('admin.admin_master')
@section('admin')

<div class="content-wrapper">
    <div class="container-full">
    <section class="content">

    <!-- Basic Forms -->
    <div class="box">
    <div class="box-header with-border">
      <h4 class="box-title">Add User</h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
      <div class="col">
        <form method="POST" action="{{route ('user.store')}}">
          @csrf
          <div class="row">
          <div class="col-12">						
            <div class="form-group">
              <h5>User Name <span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="text" name="name" id="name" class="form-control" required> </div>
            </div>

            <div class="form-group">
              <h5>Email <span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="email" name="email" id="email" class="form-control" required> </div>
            </div>

            <div class="form-group">
              <h5>Password <span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="password" name="password" id="password" class="form-control" required> </div>
            </div>

          </div>
          <div class="col-12">
            <div class="form-group">
              <h5>User Type <span class="text-danger">*</span></h5>
              <div class="controls">
                <select name="usertype" id="usertype" required class="form-control">
                  <option value="" disabled selected>Select User</option>
                  <option value="admin">Admin</option>
                  <option value="user">User</option>
                </select>
              </div>
            </div>
          </div>
          </div>
          <div class="text-xs-right">
            <input type="submit" class="btn btn-rounded btn-info mb-5" value="submit">
          </div>
        </form>

      </div>
      <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->

    </section>
    </div>
</div>

@endsection